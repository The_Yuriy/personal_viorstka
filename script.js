const scrollDiv = document.getElementById('scrollDiv');

function scrollFwd () {
    scrollDiv.scrollBy({ left: scrollDiv.clientWidth, behavior: 'smooth' });
}
function scrollBwrd () {
    scrollDiv.scrollBy({ left: -scrollDiv.clientWidth, behavior: 'smooth' });
}
